.286
dane  segment

;------------------------------
;część używana przez cały program
;------------------------------
args db 128 dup (0)              ;argumenty
arg_index  dw 64 dup('$')         ;offsety kolejnych argumentow
arg_number db 0,10,13,'$'        ;liczba argumentów


;---------------------------
;część pamięci używana przez check_args
;---------------------------
enter_code db 10,13,'$'
check_args_cfg db 2                             ;ile ma byc arg?
		   db 1,1                           ;arg 1: ile znaków (od, do)
		   db 1 							;ile jest zakresow poprawnych danych dla tego arg
		   db '0','1'                       ;zakres poprawnych znakow argumentu (od, do)
		   db 32,32                         ;arg2:ile znaków
		   db 2								;ile jest poprawnych zakresów znaków
		   db '0','9'						;pierwszy zakres
		   db 'a','f'						;drugi zakres
check_args_arg_counter db ?
arg_iterator dw ?
arg_index_iterator dw ?


;------------------------
;plansza fingerprint
;------------------------
fp_board db '+'
         db 17 dup ('-')
		 db '+',10,13,'|'
row1	 db 17 dup (0),'|',10,13,'|'
row2	 db 17 dup (0),'|',10,13,'|'
row3	 db 17 dup (0),'|',10,13,'|'
row4	 db 17 dup (0),'|',10,13,'|'
row5	 db 17 dup (0),'|',10,13,'|'
row6	 db 17 dup (0),'|',10,13,'|'
row7	 db 17 dup (0),'|',10,13,'|'
row8	 db 17 dup (0),'|',10,13,'|'
row9	 db 17 dup (0),'|',10,13,'+'
		 db 17 dup ('-')
		 db '+',10,13,'$'
;------------------------------	 
;dane do chodzenia po planszy
;------------------------------
pos_x    db 0    ;położenie gońca (wsp x)
pos_y    db 0    ;położenie gońca (wsp y)
hash     db 16 dup (?)  ;ciąg bajtów z argumentu
map      db ' .o+=*BOX@%&#/^' ;mapa do ascii art
extended_map db 50 dup ('^')
;---------
;BŁĘDY
;---------
no_arg_err db 'blad: brak argumentow',10,13,'$'
wrong_arg_num_err db 'blad: zla liczba argumentow',10,13,'$'
wrong_char_err db 'blad: wczytano niepoprawny znak',10,13,'$'
wrong_arg_len_err db 'blad: zla dlugosc argumentu ',10,13,'$'

dane ends

code  segment

print_and_exit:
  mov ax,  dane
  mov ds,ax
  mov ah,9h
  int 21h
  mov ax,4c00h
  int 21h
ret
;koniec print_and_EXIT

args2mem: ;przesuwa argumenty do args w  mencie danych
;UZYWANE REJESTRY
;AL - WCZYTYWANE argumentY
;AH - BUFOR DO CHWILOWEJ ZMIANY WARTOSCI CX
;BL - FLAGA DO POMIJANIA SPACJI
;BH - LICZNIK argumentÓW
;CX - LICZNIK PETLI/OFFSET DO argumentU (trzymany na stosie)
;DX - OFFSET ARG_INDEX
push ax
push bx
push cx
push dx
push di
push si
push ds
push es
  mov ah,51h    ;
  int 21h       ;wczytuje do ds adres PSP
  mov ds,bx     ;
  mov si,80h    ;ustawiam si na liczbie argumentow
  cmp byte ptr ds:[si],0
  je end_arg2mem ;jesli brak arg - pomiń wczytywanie
  mov ax,dane
  mov es,ax
  mov di,offset args
  lodsb     ;załaduj do ah liczbe argumentow
  xor ah,ah
  mov cx,ax ;wstawia do cx dlugosc args
  inc si    ;pomija pierwszy znak argumentu (znak pusty)
  dec cx    ;pierwsza jest spacja, od razu ją pomijam
  mov bl,1  ;ustawia flagę, ze wpisano juz dollara przed argumentem 
  mov bh,1  ;ustawia liczbe argumentow na 1
  xor dx,dx
  mov dx, offset arg_index
  mov ax, offset args
  push ax        ;na wierzchu stosu trymam offset do args
  petla_a2m: 
    lodsb        ;ładuje kolejny znak z PSP
    cmp al,' '   ;sprawdz czy wczytano spacje
    jg read_char
    cmp bl,1     ;sprawdza, czy był wpisany już dolar (jesli wczytano bialy znak)
    je skip_dollar ;jesli tak, to go nie wpisuj
      mov al,'$'
      stosb      ;oddziela argumenty dolarem
      mov bl,1   ;zaznacza, że ostatnio był wpisany dolar
      inc bh     ;zwieksz ilosc argumentow
    skip_dollar:
    jmp skip_reading  ;jezeli wczytano bialy znak
    read_char:
    mov ah,cl         ;spamietaj w ah licznik znakow do sczytania
    pop cx			;
    inc cx			;powieksza offset argumentu
    push cx			;
    xor cx,cx
    mov cl,ah        
    cmp bl,1
    jne skip_next_arg
      pop cx             ;wczytaj do cx offset argumentow
      mov bl,0   ;odznacz flagę
      push di
      mov di,dx    ;do di wpisz offset arg_index (powiekszony o ilosc argumentow)
      dec cx
      mov word ptr es:[di],cx  ;wpisuje wskaznik na kolejny argument
      add cx,2
      add di,2                 ;przesun offset na arg_index o dlugosc slowa
      mov dx, di               ;offset do arg_index spamietuje w dx
      pop di                   ;do di wraca offset na args
      push cx
      xor cx,cx
      mov cl,ah                ;do cl wraca licznik znakow do wczytania
    skip_next_arg:
    stosb                    ;wloz znak z al do args
  	
    skip_reading:
  loop petla_a2m
  pop cx
  mov al,'$'    ;
  stosb         ;na ostatni znak w args ustaw $
  mov si,dx
  mov word ptr es:[si],cx
  mov al,bh
  mov di,offset arg_number
  stosb 
end_arg2mem:
pop es
pop ds
pop si
pop di
pop dx
pop cx
pop bx
pop ax
ret

; KONIEC ARGS2MEM




print_args:;drukuje argumenty z enterami pomiedzy
push ax
push cx
push dx
push ds
push si
  mov ax,  dane
  mov ds, ax
  mov si, offset arg_number
  xor ax,ax
  lodsb
  mov cx,ax    ;zczytuje ilosc argumentow
  mov si, offset arg_index
  pa_foreach_arg:
    xor ax,ax
    lodsw      ;wczytaj offset kolejnego argumentu
    mov dx,ax  ;przepisz go do dx
    mov ah,9h
    int 21h    ;wypisz argument
    mov dx,offset enter_code
    int 21h     ;wypisz enter(10,13,'$')
  loop pa_foreach_arg
pop si
pop ds
pop dx
pop cx
pop ax
ret
; KONIEC PRINT_ARGS


check_args: ;sprawda poprawnosc args
push ax
push bx
push cx
push ds
push si
push dx

  xor cx,cx
  mov ax,   dane
  mov ds, ax
  mov si, offset check_args_cfg
  lodsb          ;ładujemy prawidłową ilosc arg z check_args_cfg    
  mov bx,si
  mov ah,ds:[arg_number]  ;ładujemy rzeczywistą ilosc arg
  cmp ah,al     ;sprawdza, czy zgadza się ilość argumentów
  mov dx,offset wrong_arg_num_err
  jne print_and_exit       ;wyrzuca blad i konczy program
  mov word ptr ds:[arg_iterator],0
  mov word ptr ds:[arg_index_iterator],0
  mov cl,al
  check_args_foreach_arg:   ;w cx znajduje się liczba parametrów
  
    lodsb          ;ładuje pierwsza czesc zakresu
    mov ah,al      
    lodsb				;argument moze miec od ah do al znakow (wlacznie)
    mov bx,ax      ;przesuwam zakres do bx
    xor ax,ax
    push si
    mov si,offset arg_index  ;w si znajduje się offset do offsetów kolejnych argumentów
    add si,word ptr ds:[arg_index_iterator] ;przesuwam si o liczbe przerobionych argumentow         
    add si,word ptr ds:[arg_index_iterator] ;przesuwam si o liczbe przerobionych argumentow (arg index są słowami, więc robię to 2 razy)        
    lodsw
   
    push bx                    ;na chwilę spamiętuję zakres na stosie
    mov bx,ax					;do bx przekladam offset aktualnego argumentu
    lodsw   ;do ax wkladam offset kolejnego argumentu
    sub ax,bx   ;w ax wpisuje dlugosc argumentu (liczoną wraz z kończącym dolarem)
    dec ax
    pop bx      ;do bx przywracam zakres
    pop si
    cmp al,bh    ;porownuję czy zgadza się dlługość argumentu
	mov dx,offset wrong_arg_len_err
    jl print_and_exit
    cmp al,bl
    jg print_and_exit
    push cx
    mov ah,al    ;liczbe znakow w argumencie przestawiam do ah
    lodsb        ;ładuję z check_args_cfg liczbę zakresów dla arfumentu
    mov ds:[check_args_arg_counter],0
    xor cx,cx
    mov bh,ah    ;robię kopię ilości znaków w bh
    mov cl,al    ;wstawiam do cx ilosc zakresow poprawnych znakow dla tego argumentu
    check_args_foreach_range:
      push cx
      mov cl,bh ;wstawiam do cx ilosc znakow w argumencie
	  lodsb      ;
	  mov ah,al  ;ładuję przedział poprawnych znaków do ax (przedział jest od ah do al) 
	  lodsb
      push bx
	  mov bx,ax  ;przesuwam zakres do bx
	  push si
	  mov si,offset arg_index
	  add si,word ptr ds:[arg_index_iterator]
	  add si,word ptr ds:[arg_index_iterator]
	  mov si,ds:[si]						;przesuwam si na pierwszy znak argumentu
	  check_args_foreach_char:
	    lodsb
	    cmp al,bh
	    jl skip_char      ;jeżeli znak jest poza zakresem, nie licz go
	      cmp al,bl
	      jg skip_char    ;------||-------
	        inc ds:[check_args_arg_counter]  ;dodaj znak do zliczonych
	    skip_char:
      loop check_args_foreach_char
	  pop si
	  pop bx      ;do bh wraca ilość znaków w argumencie
	  pop cx
    loop check_args_foreach_range
    cmp bh,ds:[check_args_arg_counter]  ;sprawdzam, czy liczba znaków z poprawnych przedziałów zgadza się z długością argumentu
	mov dx,offset wrong_char_err
    jne print_and_exit
    pop cx
    inc ds:[arg_index_iterator]
  loop check_args_foreach_arg
pop dx
pop si
pop ds
pop cx
pop bx
pop ax
ret
;KONIEC check_args



args2hash:   ;zamienia drugi argument (32 znaki hex) na 16 bajtów i wstawia do hash
push ax
push cx
push di
push si
push ds
push es
  mov ax, dane
  mov ds,ax
  mov si,offset args
  add si,2            ;ds:si wskazuje na args
  mov es,ax
  mov di,offset hash
  mov cx,16           ;es:di wskazuje na tablicę hash
  xor ax,ax
  a2h_foreach_byte:
    lodsb        ;
    mov ah,al    ;wczytaj dwie pierwsze cyfry w HEX
    lodsb        ;
    cmp ah, 58   ;sprawdz, czy to cyfra (dolne ograniczenie zapewnione przez check_args)
    jl digit
    sub ah,39    ;jeżeli to jest litera, odejmij dodatkowo 39 ('a'-39-48=10  -> a będzie równe 10, b 11 itd.)
    digit:
    sub ah,48    ;jeśli to jest cyfra, odejmij '0'
    cmp al, 58   ;To samo dla al
    jl digit2
      sub al,39    
    digit2:
      sub al,48  
    shl ah,4     ;przesunięcie liczby z ah na 4 pierwsze bity
    add al,ah    ;połączenie pierwszych 2 znaków do al
    stosb        ;wpisanie do hashu
  loop a2h_foreach_byte
pop es
pop ds
pop si
pop di
pop cx
pop ax
ret
;koniec args2hash




   ;tu zaczyna się proc przesunięcia gońca
move_left:              ;ruch w lewo
cmp ds:[pos_x],0        ;jeśli jest przy lewej ścianie
je left_thru_wall       ;idz do skakania przez sciane w lewo
dec ds:[pos_x]			;jesli nie, zmniejsz pozycje x
jmp return_from_move2   ;wroc do hash2board

move_right:             ;analogicznie ruch w prawo
cmp ds:[pos_x],16
je right_thru_wall
inc ds:[pos_x]
jmp return_from_move2

move_up:                ;analogicznie ruch w gore
cmp ds:[pos_y],0
je up_thru_wall
dec ds:[pos_y]
jmp  return_from_move 

move_down:               ;analogicznie ruch w dol
cmp ds:[pos_y],8
je down_thru_wall
inc ds:[pos_y]
jmp  return_from_move 

left_thru_wall:           ;skakanie przez sciane w lewo
cmp ds:[args],'0'         ;jesli pierwszym argumentem bylo 0
je return_from_move2      ;nie ruszaj sie w poziomie i wroc do hash2board
mov ds:[pos_x],16         ;jesli to nie bylo 0, zmien poz_x na ostatnia kolumne
jmp return_from_move2     ;wroc do hash2board    

right_thru_wall:          ;analogicznie skok w prawo
cmp ds:[args],'0'
je return_from_move2
mov ds:[pos_x],0
jmp return_from_move2

up_thru_wall:             ;analogicznie skok w gore
cmp ds:[args],'0'
je  return_from_move 
mov ds:[pos_y],8
jmp  return_from_move 

down_thru_wall:           ;analogicznie skok w dol
cmp ds:[args],'0'
je return_from_move 
mov ds:[pos_y],0
jmp  return_from_move 
;koniec wszystkich labeli move
;z uwagi na ruch zalezny od bitow, move nie jest wywolywany przez CALL, tylko JMP


mark_on_board:            ;procedura zaznaczajaca aktualna pozycje na planszy
push ax
push cx
push di
push ds
  mov ax,dane
  mov ds,ax
  mov di,offset row1        ;przesun di, zeby wskazywalo pierwszy wiersz
  xor cx,cx
  xor ax,ax
  mov cl,byte ptr  ds:[pos_y] ;ustaw w liczniku petli nr rzedu, w ktorym jest goniec
  mark_foreach_row:
    cmp cl,0
    je skip_foreach
    add di,21						;przesuń di, żeby wskazywało dobry rząd (21*pos_y), gdzie 21 jest dlugoscia wiersza
    dec cl							
  jmp mark_foreach_row
  skip_foreach:
  mov al,byte ptr ds:[pos_x]          ;przesuwa do ax pozycje gonca w poziomie
  add di,ax						   ;ds:di wskazuje na aktualną pozycję gońca
  inc byte ptr ds:[di]                ;powieksza zapisaną w tablicy ilosc odwiedzen
pop ds
pop di
pop cx
pop ax
ret
skip_marking:
;koniec procedury zaznaczającej ruch


ASCII_Art:
push ax
push cx
push di
push si
push ds
  mov ax,dane
  mov ds,ax
  mov di,offset map        ;ds:di wskazuje na miejsce, z których będą brane znaki do wstawiania
  mov si, offset row1      ;si ustawia się na pierwsza komorke do zamiany (lewy górny róg)
  mov cx,9
  xor ax,ax
  AA_foreach_row:         
    push cx                       ;zapamietaj licznik rzedow
    mov cx,17
    AA_foreach_char:
      mov al,byte ptr ds:[si]     ;wczytaj znak z planszy
	  add di,ax                   ;przesun sie do odpowiedniego miejsca w mapie
	  mov al,byte ptr ds:[di]     ;wpisz do al odpowiedni znak
	  mov byte ptr ds:[si],al     ;wpisz na plansze odpowiedni znak
	  inc si                      ;przejdz na kolejne pole
	  mov di,offset map           ;wróć ds:di na poczatek "mapy"
    loop AA_foreach_char
    add si,4                      ;przesuń si do nastepnego rzedu
    pop cx                        ;przywroc do cx licxnik rzedow
  loop AA_foreach_row
  mov si,offset row5         ;
  add si,8				   ;zaznaczenie startu
  mov byte ptr ds:[si],'S'   ;
  xor ax,ax
  mov di, offset row1
  mov cl,byte ptr  ds:[pos_y]
  AA_foreach_row2:
    cmp cl,0
    je AA_skip_foreach
    add di,21							;przesuń si, żeby wskazywało rząd konca ruchu
    loop AA_foreach_row2
  AA_skip_foreach:
  mov al,byte ptr ds:[pos_x]           ;przesun si, zeby wskazywalo pozycje konca ruchu
  add di,ax
  mov byte ptr ds:[di],'E'             ;zaznacz koniec ruchow na planszy literka 'E'
pop ds
pop si
pop di
pop cx
pop ax
ret
;koniec ASCII-Art

hash2board:  ;głowna funkcja rysująca na planszy fingerprint
push ax
push bx
push cx
push si
push ds
  mov ax,dane
  mov ds,ax
  mov byte ptr ds:[pos_x],8     ;
  mov byte ptr ds:[pos_y],4     ;Ustaw się na starcie
  mov si,offset hash            ;ustaw si do sczytywania bajtów
  mov cx,16
  h2b_foreach_byte:
    push cx
    lodsb                       ;załaduj kolejny bajt
    mov cx,4
    h2b_foreach_bitpair:
      mov bl,al
      and bl,3       ;w bx znajduje sie interesujaca nas para bitow
      mov bh,bl
	  and bh,2       ;w bh znajduje się pierwszy bit,
	  and bl,1       ; a w dl drugi
	  cmp bh,2       ;sprawdz czy ruch ma byc w gore, czy w dol
	  je move_down
	  jmp move_up
	  return_from_move:
	  cmp bl,1       ;sprawdz, czy ruch ma byc w lewo czy w prawo
	  je move_right
	  jmp move_left
	  return_from_move2:
	  call mark_on_board  ;zaznacz odwiedzenie po wykonaniu ruchu
      shr al,2       ;przesun ax o dwa zuzyte bity
    loop h2b_foreach_bitpair
    pop cx
  loop h2b_foreach_byte
  call ASCII_Art        ;zamien ilosci odwiedzin na odpowiednie znaki
pop ds
pop si
pop cx
pop bx
pop ax
ret   ;koniec hash2board




;-----------------------------------------
;POCZATEK PROGRAMU
;-----------------------------------------
start1:
mov ax,stos1
mov ss,ax
mov sp,offset w_stosu ;inicjacja stosu
call args2mem         ;przesun argumenty do pamieci
;call print_args
call check_args           ;sprawdz poprawnosc argumentow
call args2hash        ;zamien wczytany drugi argument na z szesnastkowego na bajty
call hash2board       ;wykonaj ruchy i zaznacz ilosc odwiedzin
mov dx,offset fp_board 
jmp print_and_exit    ;WYPISZ planszę i zakończ program
code ends

stos1  segment stack
dw 200 dup (?)
w_stosu dw 0
stos1 ends
end start1